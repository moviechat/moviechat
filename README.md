# MovieChat Public Repo #

### What is this repository for? ###

* This is a public repo for [MovieChat](https://moviechat.org), the popular movie discussion platform.
* Public, open source code and APIs will be hosted here
* Learn more about MovieChat and the project: [About](https://moviechat.org/about)

### Contact ###

* [Contact](https://moviechat.org/contact)